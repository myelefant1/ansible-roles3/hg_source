## hg_source

This role is a frontend of mercurial module of ansible.
  * configures timestamp history

## Role parameters

| name                  | value       | default_value   | description                 |
| ----------------------|-------------|-----------------|-----------------------------|
| hg_source_url         | string      | no              | url of mercurial repository |
| hg_source_target      | string      | no              | target dir for checkout     |
| hg_source_revision    | string      | no              | revision                    |
| hg_source_user        | string      | no              | user 

